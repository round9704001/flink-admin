package cn.chongho.inf.flink.mapper;

import cn.chongho.inf.flink.model.DbTableColumn;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ming
 */
public interface DbTableColumnMapper extends Mapper<DbTableColumn> {
}
