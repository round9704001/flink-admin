package cn.chongho.inf.flink.mapper;

import cn.chongho.inf.flink.model.Cluster;
import tk.mybatis.mapper.common.Mapper;


/**
 * @author ming
 */
public interface ClusterMapper extends Mapper<Cluster> {

}
